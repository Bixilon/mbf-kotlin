/*
 * Minosoft
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

package de.bixilon.mbf.tests

import de.bixilon.mbf.*
import org.junit.jupiter.api.Assertions.assertArrayEquals
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import kotlin.test.Test
import kotlin.test.assertEquals

internal class SpecificationExampleTest {
    private val firstExampleBinary = byteArrayOf(
        0x4D, 0x42, 0x46,
        0x00,

        0x00,

        0x12,

        0x00, 0x00, 0x00, 0x02,


        0x0A,
        0x00, 0x00, 0x00, 0x04,
        0x6E, 0x61, 0x6D, 0x65,

        0x0A,
        0x00, 0x00, 0x00, 0x06,
        0x4D, 0x6F, 0x72, 0x69, 0x74, 0x7A,


        0x0A,
        0x00, 0x00, 0x00, 0x03,
        0x61, 0x67, 0x65,

        0x04,
        0x00, 0x00, 0x00, 0x11,
    )

    private val firstExampleExpected = MBFData(
        dataInfo = MBFDataInfo(
            MBFCompressionTypes.NONE,
            encryption = false,
            variableLengthPrefix = false,
            preferVariableTypes = false,
            version = 0,
        ),
        data = mutableMapOf<Any, Any>(
            "name" to "Moritz",
            "age" to 17,
        )
    )

    private val secondExampleBinary = byteArrayOf(
        0x4D, 0x42, 0x46,

        0x00,

        0x08,

        0x12,

        0x04,


        0x0A,
        0x04,
        0x6E, 0x61, 0x6D, 0x65,

        0x0A,
        0x06,
        0x4D, 0x6F, 0x72, 0x69, 0x74, 0x7A,


        0x0A,
        0x03,
        0x61, 0x67, 0x65,

        0x04,
        0x00, 0x00, 0x00, 0x11,


        0x0A,
        0x07,
        0x68, 0x6F, 0x62, 0x62, 0x69, 0x65, 0x73,

        0x0D,
        0x03,
        0x0A,

        0x08,
        0x53, 0x6C, 0x65, 0x65, 0x70, 0x69, 0x6E, 0x67,

        0x06,
        0x43, 0x6F, 0x64, 0x69, 0x6E, 0x67,

        0x15,
        0x44, 0x65, 0x76, 0x65, 0x6C, 0x6F, 0x70, 0x69, 0x6E, 0x67, 0x20, 0x63, 0x72, 0x61, 0x7A, 0x79, 0x20, 0x73, 0x68, 0x69, 0x74,


        0x0A,
        0x0B,
        0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6F, 0x6E,

        0x0A,
        0xA4.toByte(), 0x02,
        0x48, 0x69, 0x20, 0x74, 0x68, 0x65, 0x72, 0x65, 0x2C, 0x20, 0x6D, 0x79, 0x20, 0x6E, 0x61, 0x6D, 0x65, 0x20, 0x69, 0x73, 0x20, 0x4D, 0x6F, 0x72, 0x69, 0x74, 0x7A,
        0x2C, 0x20, 0x49, 0x20, 0x64, 0x6F, 0x20, 0x63, 0x72, 0x61, 0x7A, 0x79, 0x20, 0x73, 0x68, 0x69, 0x74, 0x2C, 0x20, 0x6C, 0x69, 0x6B, 0x65, 0x20, 0x64, 0x65, 0x76,
        0x65, 0x6C, 0x6F, 0x70, 0x69, 0x6E, 0x67, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20, 0x66, 0x6F, 0x72, 0x6D, 0x61, 0x74, 0x2E, 0x20, 0x49, 0x74, 0x20, 0x77, 0x61, 0x73,
        0x20, 0x6A, 0x75, 0x73, 0x74, 0x20, 0x61, 0x6E, 0x20, 0x69, 0x64, 0x65, 0x61, 0x2C, 0x20, 0x49, 0x20, 0x6E, 0x65, 0x65, 0x64, 0x65, 0x64, 0x20, 0x73, 0x6F, 0x6D,
        0x65, 0x74, 0x68, 0x69, 0x6E, 0x67, 0x20, 0x74, 0x6F, 0x64, 0x6F, 0x2C, 0x20, 0x62, 0x75, 0x74, 0x20, 0x6E, 0x6F, 0x77, 0x20, 0x79, 0x6F, 0x75, 0x20, 0x73, 0x65,
        0x65, 0x20, 0x74, 0x68, 0x69, 0x73, 0x2C, 0x20, 0x73, 0x6F, 0x20, 0x49, 0x20, 0x6D, 0x69, 0x67, 0x68, 0x74, 0x20, 0x62, 0x6C, 0x6F, 0x77, 0x20, 0x79, 0x6F, 0x75,
        0x72, 0x20, 0x6D, 0x69, 0x6E, 0x64, 0x2E, 0x20, 0x49, 0x20, 0x61, 0x6D, 0x20, 0x61, 0x6C, 0x73, 0x6F, 0x20, 0x77, 0x6F, 0x72, 0x6B, 0x69, 0x6E, 0x67, 0x20, 0x6F,
        0x6E, 0x20, 0x6D, 0x69, 0x6E, 0x65, 0x63, 0x72, 0x61, 0x66, 0x74, 0x20, 0x73, 0x74, 0x75, 0x66, 0x66, 0x2C, 0x20, 0x6C, 0x69, 0x6B, 0x65, 0x20, 0x77, 0x72, 0x69,
        0x74, 0x69, 0x6E, 0x67, 0x20, 0x6D, 0x79, 0x20, 0x6F, 0x77, 0x6E, 0x20, 0x63, 0x6C, 0x69, 0x65, 0x6E, 0x74, 0x2E, 0x20, 0x50, 0x72, 0x65, 0x74, 0x74, 0x79, 0x20,
        0x75, 0x6E, 0x75, 0x73, 0x75, 0x61, 0x6C, 0x2C, 0x20, 0x62, 0x75, 0x74, 0x20, 0x79, 0x61, 0x68, 0x2E, 0x20, 0x49, 0x20, 0x74, 0x68, 0x69, 0x6E, 0x6B, 0x20, 0x49,
        0x20, 0x68, 0x61, 0x76, 0x65, 0x20, 0x74, 0x6F, 0x6F, 0x20, 0x6D, 0x75, 0x63, 0x68, 0x20, 0x74, 0x69, 0x6D, 0x65, 0x20, 0x3A, 0x29,
    )

    private val gzipBinary = byteArrayOf(
        77, 66, 70, 0, 10, -102, 2, 31, -117, 8, 0, 0, 0, 0, 0, 0, 0, 77, -112, -63, 74, 3, 49, 16, -122, -117, 45, 85, 86, 60, 40, 120, -1, -81, -62, -30, 3, 120, -43, -125,
        61, 8, -126, 79, -112, -35, -52, 110, 98, 55, -103, -110, 76, -70, 108, -97, -57, 7, 117, 98, 5, 61, 13, -55, -4, -13, -51, -105, -36, 109, -102, 77, 52, -127, -102,
        -19, 27, 39, 47, -89, 102, 109, 70, -38, -84, 86, -85, -37, -26, -46, 113, -41, 121, -54, 55, -21, -26, -22, 99, 34, 58, -8, 56, 110, -97, -39, 106, -71, 127, -95, 35,
        77, 92, 111, -48, 39, 115, 90, -112, -99, -105, -26, -38, 82, -18, -109, 63, -120, -25, -40, 124, 93, -68, 122, -120, -93, 68, 45, -62, -126, -70, 4, 62, -29, -68,
        -90, -59, 14, -106, -1, -51, -74, -104, -4, -98, 96, -1, -72, -30, 52, 61, 112, 10, 70, 30, -79, 19, -52, 38, -29, -77, 100, -127, -119, -16, -106, 76, 69, 68, 34,
        75, 22, -103, 3, 105, -68, 14, -79, -27, 22, 93, 17, 68, -98, -79, 112, 65, 38, -6, 65, -75, -102, -46, -119, -32, 71, 39, -24, -90, 115, 55, -23, 57, 90, -59, -61,
        4, -104, 73, 19, 51, -89, 125, 5, 113, -84, 45, 82, -63, 65, -112, -91, 12, -61, -81, -31, -84, -6, 53, -96, 79, -30, 57, -94, -97, 60, 69, 21, 124, 79, 36, -78, -96,
        -60, -110, -117, -103, -50, 10, -117, 113, 21, 93, -51, -10, 90, -99, 57, -86, 10, 51, 66, -23, 29, -60, -21, 127, 60, 61, 124, 3, 8, 58, 26, -96, -128, 1, 0, 0,
    )

    private val deflateBinary = byteArrayOf(
        77, 66, 70, 0, 11, -114, 2, 120, -100, 77, -112, -63, 74, 3, 49, 16, -122, -117, 45, 85, 86, 60, 40, 120, -1, -81, -62, -30, 3, 120, -43, -125, 61, 8, -126, 79, -112,
        -35, -52, 110, 98, 55, -103, -110, 76, -70, 108, -97, -57, 7, 117, 98, 5, 61, 13, -55, -4, -13, -51, -105, -36, 109, -102, 77, 52, -127, -102, -19, 27, 39, 47, -89, 102,
        109, 70, -38, -84, 86, -85, -37, -26, -46, 113, -41, 121, -54, 55, -21, -26, -22, 99, 34, 58, -8, 56, 110, -97, -39, 106, -71, 127, -95, 35, 77, 92, 111, -48, 39, 115,
        90, -112, -99, -105, -26, -38, 82, -18, -109, 63, -120, -25, -40, 124, 93, -68, 122, -120, -93, 68, 45, -62, -126, -70, 4, 62, -29, -68, -90, -59, 14, -106, -1, -51, -74,
        -104, -4, -98, 96, -1, -72, -30, 52, 61, 112, 10, 70, 30, -79, 19, -52, 38, -29, -77, 100, -127, -119, -16, -106, 76, 69, 68, 34, 75, 22, -103, 3, 105, -68, 14, -79, -27,
        22, 93, 17, 68, -98, -79, 112, 65, 38, -6, 65, -75, -102, -46, -119, -32, 71, 39, -24, -90, 115, 55, -23, 57, 90, -59, -61, 4, -104, 73, 19, 51, -89, 125, 5, 113, -84, 45,
        82, -63, 65, -112, -91, 12, -61, -81, -31, -84, -6, 53, -96, 79, -30, 57, -94, -97, 60, 69, 21, 124, 79, 36, -78, -96, -60, -110, -117, -103, -50, 10, -117, 113, 21, 93, -51,
        -10, 90, -99, 57, -86, 10, 51, 66, -23, 29, -60, -21, 127, 60, 61, 124, 3, -23, 88, -126, 31,
    )

    private val zsdtBinary = byteArrayOf(
        77, 66, 70, 0, 9, -101, 2, 40, -75, 47, -3, 96, -128, 0, -115, 8, 0, 86, 83, 58, 38, 112, 73, -101, 3, -4, -1, 127, -79, 89, 127, -110, 77, -65, -56, -34, -113, 66, -74, -123,
        108, 19, 15, -26, -48, -103, 11, 52, -66, 6, 80, 75, -115, 101, -58, -97, 63, -78, 22, 54, 0, 45, 0, 44, 0, 13, -52, 53, -112, 90, 25, 124, -12, 80, -81, 57, -73, 116, -28, -62,
        -63, -77, -70, 88, 97, -15, -84, 54, 50, -37, -125, 64, 74, 16, -64, 84, -122, 97, 22, -112, 10, 16, 4, 81, -28, -93, 18, -23, -47, -6, -72, 97, -111, -46, -87, 85, 36, -123, 65,
        -71, 50, 87, -27, -46, -81, 122, -61, 8, 113, -88, -96, -9, 114, -91, -107, 123, -20, 28, 42, -21, -11, 100, 104, 123, -62, -21, -53, -109, 67, -3, 35, -9, 36, 95, -78, -66, 50,
        0, 68, -92, 101, -19, -122, -66, -19, -88, -41, -28, 50, -29, 49, 116, -122, 77, -35, -87, 47, 87, -42, -86, -5, 107, -52, -101, 61, -22, -66, -100, -31, -109, 76, -8, -56, -47,
        33, -34, 119, 100, -98, -16, -30, -2, -77, -54, 58, 29, -61, -81, 8, 9, 79, -89, 125, -42, 68, 119, -26, 126, 58, 20, -5, 114, -58, 42, -21, 47, 87, -104, -118, -102, -118, 48,
        110, 124, -44, -99, -49, -31, 74, 111, 106, 107, -66, 16, 63, 114, -83, 15, 114, -100, -40, -16, 1, 13, 0, 25, -52, 56, -94, -127, -108, 76, 38, 31, -61, 24, 108, -101, -96, 83,
        -19, -60, 17, 24, 97, -55, 22, 118, -4, 106, -96, 26, -109, -127, -122, 40, 93, -63, -36, 3,
    )

    val secondExampleExpected = MBFData(
        dataInfo = MBFDataInfo(
            MBFCompressionTypes.NONE,
            encryption = false,
            variableLengthPrefix = true,
            preferVariableTypes = false,
            version = 0,
        ),
        data = mutableMapOf<Any, Any>(
            "name" to "Moritz",
            "age" to 17,
            "hobbies" to mutableListOf(
                "Sleeping",
                "Coding",
                "Developing crazy shit",
            ),
            "description" to "Hi there, my name is Moritz, I do crazy shit, like developing this format. It was just an idea, I needed something todo, but now you see this, so I might blow your mind. I am also working on minecraft stuff, like writing my own client. Pretty unusual, but yah. I think I have too much time :)"
        )
    )

    @Test
    fun testFirstReading() {
        val reader = MBFBinaryReader(ByteArrayInputStream(firstExampleBinary))

        assertEquals<Any>(firstExampleExpected, reader.readMBF())
    }

    @Test
    fun testFirstWriting() {
        val outputStream = ByteArrayOutputStream()
        MBFBinaryWriter(outputStream).writeMBF(firstExampleExpected)

        assertArrayEquals(firstExampleBinary, outputStream.toByteArray())
    }

    @Test
    fun testSecondReading() {
        val reader = MBFBinaryReader(ByteArrayInputStream(secondExampleBinary))

        assertEquals<Any>(secondExampleExpected, reader.readMBF())
    }

    @Test
    fun testSecondWriting() {
        val outputStream = ByteArrayOutputStream()
        MBFBinaryWriter(outputStream).writeMBF(secondExampleExpected)

        assertArrayEquals(secondExampleBinary, outputStream.toByteArray())
    }

    @Test
    fun testDeflateWriting() {
        val outputStream = ByteArrayOutputStream()
        MBFBinaryWriter(outputStream).writeMBF(secondExampleExpected.copy(dataInfo = secondExampleExpected.dataInfo.copy(compression = MBFCompressionTypes.DEFLATE)))

        assertArrayEquals(deflateBinary, outputStream.toByteArray())
    }

    @Test
    fun testDeflateReading() {
        val reader = MBFBinaryReader(ByteArrayInputStream(deflateBinary))

        assertEquals<Any>(secondExampleExpected.copy(dataInfo = secondExampleExpected.dataInfo.copy(compression = MBFCompressionTypes.DEFLATE)), reader.readMBF())
    }

    @Test
    fun testGzipWriting() {
        val outputStream = ByteArrayOutputStream()
        MBFBinaryWriter(outputStream).writeMBF(secondExampleExpected.copy(dataInfo = secondExampleExpected.dataInfo.copy(compression = MBFCompressionTypes.GZIP)))

        assertArrayEquals(gzipBinary, outputStream.toByteArray())
    }

    @Test
    fun testGzipReading() {
        val reader = MBFBinaryReader(ByteArrayInputStream(gzipBinary))

        assertEquals<Any>(secondExampleExpected.copy(dataInfo = secondExampleExpected.dataInfo.copy(compression = MBFCompressionTypes.GZIP)), reader.readMBF())
    }

    @Test
    fun testZsdtWriting() {
        val outputStream = ByteArrayOutputStream()
        MBFBinaryWriter(outputStream).writeMBF(secondExampleExpected.copy(dataInfo = secondExampleExpected.dataInfo.copy(compression = MBFCompressionTypes.ZSDT)))

        assertArrayEquals(zsdtBinary, outputStream.toByteArray())
    }

    @Test
    fun testZsdtReading() {
        val reader = MBFBinaryReader(ByteArrayInputStream(zsdtBinary))

        assertEquals<Any>(secondExampleExpected.copy(dataInfo = secondExampleExpected.dataInfo.copy(compression = MBFCompressionTypes.ZSDT)), reader.readMBF())
    }
}
