/*
 * Minosoft
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

package de.bixilon.mbf

import com.github.luben.zstd.Zstd
import de.bixilon.mbf.MBFUtil.generics
import java.io.ByteArrayOutputStream
import java.io.OutputStream
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.zip.Deflater
import java.util.zip.GZIPOutputStream

class MBFBinaryWriter(
    private val output: OutputStream,
) {
    var variableLengthPrefix = false
    var preferVariableTypes = false

    fun writeByte(byte: Byte) {
        output.write(byte.toInt())
    }

    fun writeByte(byte: Int) {
        output.write(byte)
    }

    fun writeByte(char: Char) {
        writeByte(char.code.toByte())
    }

    fun writeByteArray(byteArray: ByteArray) {
        writeLength(byteArray.size)

        writeRawByteArray(byteArray)
    }

    fun writeRawByteArray(byteArray: ByteArray) {
        output.write(byteArray)
    }

    fun writeBoolean(boolean: Boolean) {
        if (boolean) {
            writeByte(0x01)
        } else {
            writeByte(0x00)
        }
    }

    fun writeBooleanArray(booleanArray: BooleanArray) {
        writeLength(booleanArray.size)
        for (boolean in booleanArray) {
            writeBoolean(boolean)
        }
    }

    fun writeShort(short: Short) {
        writeShort(short.toInt())
    }

    fun writeShort(short: Int) {
        writeByte(short ushr Byte.SIZE_BITS)
        writeByte(short)
    }

    fun writeShortArray(shortArray: ShortArray) {
        writeLength(shortArray.size)
        for (short in shortArray) {
            writeShort(short)
        }
    }

    fun writeInt(int: Int) {
        writeShort(int ushr Short.SIZE_BITS)
        writeShort(int)
    }

    fun writeInt(int: Long) {
        writeInt(int.toInt())
    }

    fun writeIntArray(intArray: IntArray) {
        writeLength(intArray.size)
        for (int in intArray) {
            writeInt(int)
        }
    }

    fun writeLong(long: Long) {
        writeInt(long ushr Int.SIZE_BITS)
        writeInt(long)
    }

    fun writeLongArray(longArray: LongArray) {
        writeLength(longArray.size)
        for (long in longArray) {
            writeLong(long)
        }
    }

    fun writeFloat(float: Float) {
        writeInt(float.toBits())
    }

    fun writeFloatArray(floatArray: FloatArray) {
        writeLength(floatArray.size)
        for (long in floatArray) {
            writeFloat(long)
        }
    }

    fun writeDouble(double: Double) {
        writeLong(double.toBits())
    }

    fun writeDoubleArray(doubleArray: DoubleArray) {
        writeLength(doubleArray.size)
        for (long in doubleArray) {
            writeDouble(long)
        }
    }

    fun writeVarInt(int: Int) {
        var value = int
        do {
            var temp = value and 0x7F
            value = value ushr 7
            if (value != 0) {
                temp = temp or 0x80
            }
            writeByte(temp)
        } while (value != 0)
    }

    fun writeVarIntArray(intArray: IntArray) {
        writeLength(intArray.size)
        for (long in intArray) {
            writeVarInt(long)
        }
    }

    fun writeVarLong(long: Long) {
        var value = long
        do {
            var temp = value and 0x7F
            value = value ushr 7
            if (value != 0L) {
                temp = temp or 0x80
            }
            writeByte(temp.toInt())
        } while (value != 0L)
    }

    fun writeVarLongArray(longArray: LongArray) {
        writeLength(longArray.size)
        for (long in longArray) {
            writeVarLong(long)
        }
    }


    fun writeUUID(uuid: UUID) {
        writeLong(uuid.leastSignificantBits)
        writeLong(uuid.mostSignificantBits)
    }


    fun writeLength(length: Int) {
        if (variableLengthPrefix) {
            writeVarInt(length)
        } else {
            writeInt(length)
        }
    }

    fun writeString(string: String) {
        writeByteArray(string.toByteArray(StandardCharsets.UTF_8))
    }

    fun writeMBF(data: Any?) {
        val type = data.mbfType

        writeByte(type.ordinal)
        type.write(this, data)
    }

    fun writeMBF(data: MBFData) {
        variableLengthPrefix = data.dataInfo.variableLengthPrefix
        preferVariableTypes = data.dataInfo.preferVariableTypes
        writeByte('M')
        writeByte('B')
        writeByte('F')

        writeByte(data.dataInfo.version)

        writeByte(
            (data.dataInfo.compression.ordinal and 0b11) or
                    (data.dataInfo.encryption.toInt() shl 2) or
                    (data.dataInfo.variableLengthPrefix.toInt() shl 3) or
                    (data.dataInfo.preferVariableTypes.toInt() shl 4)
        )


        val outputStream = ByteArrayOutputStream()

        val writer = MBFBinaryWriter(outputStream)
        writer.preferVariableTypes = data.dataInfo.preferVariableTypes
        writer.variableLengthPrefix = data.dataInfo.variableLengthPrefix
        writer.writeMBF(data.data)

        var currentData = outputStream.toByteArray()

        if (data.dataInfo.encryption) {
            TODO("Encryption is not implemented yet")
        }

        if (data.dataInfo.compression != MBFCompressionTypes.NONE) {
            val compressedOutputStream = ByteArrayOutputStream()

            when (data.dataInfo.compression) {
                MBFCompressionTypes.ZSDT -> {
                    compressedOutputStream.write(Zstd.compress(currentData, data.dataInfo.compressionLevel))
                }
                MBFCompressionTypes.GZIP -> {
                    val gzipOutputStream = GZIPOutputStream(compressedOutputStream)

                    gzipOutputStream.write(currentData)

                    gzipOutputStream.close()
                }
                MBFCompressionTypes.DEFLATE -> {
                    val deflater = Deflater()
                    deflater.setInput(currentData)
                    deflater.finish()
                    val buffer = ByteArray(MBFUtil.DEFAULT_BUFFER_SIZE)
                    while (!deflater.finished()) {
                        compressedOutputStream.write(buffer, 0, deflater.deflate(buffer))
                    }
                }
                else -> TODO()
            }
            compressedOutputStream.close()

            ByteArrayOutputStream().let {
                val compressed = compressedOutputStream.toByteArray()
                val outWriter = MBFBinaryWriter(it)
                outWriter.preferVariableTypes = data.dataInfo.preferVariableTypes
                outWriter.variableLengthPrefix = data.dataInfo.variableLengthPrefix
                outWriter.writeLength(compressed.size)
                outWriter.writeRawByteArray(compressed)
                currentData = it.toByteArray()
            }
        }

        writeRawByteArray(currentData)
    }

    fun getDataType(data: Any?): MBFDataTypes {
        return data.mbfType
    }

    fun getDataTypeClass(`class`: Class<*>?): MBFDataTypes {
        return `class`.mbfType
    }


    private val Any?.mbfType: MBFDataTypes
        get() {
            return when (this) {
                null -> MBFDataTypes.NULL
                is Boolean -> MBFDataTypes.BOOLEAN
                is Byte -> MBFDataTypes.INT8
                is Short -> MBFDataTypes.INT16
                is Int -> {
                    if (preferVariableTypes) {
                        MBFDataTypes.VAR_INT
                    } else {
                        MBFDataTypes.INT32
                    }
                }
                is Long -> MBFDataTypes.INT64
                is UUID -> MBFDataTypes.INT128
                is Float -> MBFDataTypes.FLOAT
                is Double -> MBFDataTypes.DOUBLE
                is String -> MBFDataTypes.STRING
                is BooleanArray, is ByteArray, is ShortArray, is IntArray, is LongArray, is FloatArray, is DoubleArray -> MBFDataTypes.NORMAL_ARRAY
                is Array<*> -> MBFDataTypes.MIXED_ARRAY // ToDo
                is List<*> -> {
                    val generic = this.generics[0]
                    if (generic == Any::class.java) {
                        MBFDataTypes.MIXED_LIST
                    } else {
                        MBFDataTypes.NORMAL_LIST
                    }
                }
                is Set<*> -> {
                    val generic = this.generics[0]
                    if (generic == Any::class.java) {
                        MBFDataTypes.MIXED_SET
                    } else {
                        MBFDataTypes.NORMAL_SET
                    }
                }
                is Map<*, *> -> {
                    val generics = this.generics
                    if (generics[0] == Any::class.java || generics[1] == Any::class.java) {
                        MBFDataTypes.MIXED_MAP
                    } else {
                        MBFDataTypes.NORMAL_MAP
                    }
                }
                else -> TODO("MBT does not support data type ${this::class.java}!")
            }
        }

    private val Class<*>?.mbfType: MBFDataTypes
        get() {
            this ?: return MBFDataTypes.NULL
            return when (this) {
                Boolean::class.java, java.lang.Boolean::class.java -> MBFDataTypes.BOOLEAN
                Byte::class.java, java.lang.Byte::class.java -> MBFDataTypes.INT8
                Short::class.java, java.lang.Short::class.java -> MBFDataTypes.INT16
                Int::class.java, java.lang.Integer::class.java -> {
                    if (preferVariableTypes) {
                        MBFDataTypes.VAR_INT
                    } else {
                        MBFDataTypes.INT32
                    }
                }
                Long::class.java, java.lang.Long::class.java -> MBFDataTypes.INT64
                UUID::class.java -> MBFDataTypes.INT128
                Float::class.java, java.lang.Float::class.java -> MBFDataTypes.FLOAT
                Double::class.java, java.lang.Double::class.java -> MBFDataTypes.DOUBLE
                String::class.java, java.lang.String::class.java -> MBFDataTypes.STRING
                BooleanArray::class.java, ByteArray::class.java, ShortArray::class.java, IntArray::class.java, LongArray::class.java, FloatArray::class.java, DoubleArray::class.java -> MBFDataTypes.NORMAL_ARRAY
                Array::class.java -> {
                    if (this::class.java.componentType == Any::class.java) {
                        MBFDataTypes.MIXED_ARRAY
                    } else {
                        MBFDataTypes.NORMAL_ARRAY
                    }
                }
                else -> null
            } ?: when {
                List::class.java.isAssignableFrom(this) -> MBFDataTypes.MIXED_LIST
                Set::class.java.isAssignableFrom(this) -> MBFDataTypes.MIXED_SET
                Map::class.java.isAssignableFrom(this) -> MBFDataTypes.MIXED_MAP
                else -> TODO("MBT does not support data type ${this}!")
            }
        }


    companion object {
        fun Boolean.toInt(): Int {
            if (this) {
                return 0x01
            }
            return 0x00
        }
    }
}
