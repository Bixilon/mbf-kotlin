/*
 * Minosoft
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

package de.bixilon.mbf

import de.bixilon.mbf.MBFUtil.generics
import java.util.*

enum class MBFDataTypes(
    val reader: (reader: MBFBinaryReader) -> Any? = { TODO("Reading not yet implemented") },
    val write: (writer: MBFBinaryWriter, data: Any?) -> Unit = { _, _ -> TODO("Writing not yet implemented") },
) {
    NULL(
        { null },
        { _, _ -> },
    ),

    BOOLEAN(
        { it.readBoolean() },
        { writer, data -> writer.writeBoolean(data as Boolean) },
    ),
    INT8(
        { it.readByte() },
        { writer, data -> writer.writeByte(data as Byte) },
    ),
    INT16(
        { it.readShort() },
        { writer, data -> writer.writeShort(data as Short) },
    ),
    INT32(
        { it.readInt() },
        { writer, data -> writer.writeInt(data as Int) },
    ),
    INT64(
        { it.readLong() },
        { writer, data -> writer.writeLong(data as Long) },
    ),
    INT128(
        { it.readUUID() },
        { writer, data -> writer.writeUUID(data as UUID) },
    ),
    FLOAT(
        { it.readFloat() },
        { writer, data -> writer.writeFloat(data as Float) },
    ),
    DOUBLE(
        { it.readDouble() },
        { writer, data -> writer.writeDouble(data as Double) },
    ),

    VAR_INT(
        { it.readVarInt() },
        { writer, data -> writer.writeVarInt(data as Int) },
    ),

    STRING(
        { it.readString() },
        { writer, data -> writer.writeString(data as String) },
    ),


    NORMAL_ARRAY(
        {
            val length = it.readLength()

            when (val type = it.readMBFDataType()) {
                NULL -> arrayOfNulls<Any?>(length)
                BOOLEAN -> it.readBooleanArray()
                INT8 -> it.readByteArray(length)
                INT16 -> it.readShortArray(length)
                INT32 -> it.readIntArray(length)
                INT64 -> it.readLongArray(length)
                FLOAT -> it.readFloatArray(length)
                DOUBLE -> it.readDoubleArray(length)
                VAR_INT -> it.readVarIntArray(length)
                VAR_LONG -> it.readVarLongArray(length)
                else -> it.readArray(length) { type.reader(it) }
            }
        },
        { writer, data ->
            when (data) {
                null -> {
                    writer.writeLength(0)
                    writer.writeByte(NULL.ordinal)
                }
                is BooleanArray -> {
                    writer.writeLength(data.size)
                    writer.writeByte(BOOLEAN.ordinal)
                    writer.writeBooleanArray(data)
                }
                is ByteArray -> {
                    writer.writeLength(data.size)
                    writer.writeByte(INT8.ordinal)
                    writer.writeByteArray(data)
                }
                is ShortArray -> {
                    writer.writeLength(data.size)
                    writer.writeByte(INT16.ordinal)
                    writer.writeShortArray(data)
                }
                is IntArray -> {
                    writer.writeLength(data.size)
                    if (writer.preferVariableTypes) {
                        writer.writeByte(VAR_INT.ordinal)
                        writer.writeVarIntArray(data)
                    } else {
                        writer.writeByte(INT32.ordinal)
                        writer.writeIntArray(data)
                    }
                }
                is LongArray -> {
                    writer.writeLength(data.size)
                    if (writer.preferVariableTypes) {
                        writer.writeByte(VAR_LONG.ordinal)
                        writer.writeVarLongArray(data)
                    } else {
                        writer.writeByte(INT64.ordinal)
                        writer.writeLongArray(data)
                    }
                }
                is FloatArray -> {
                    writer.writeLength(data.size)
                    writer.writeByte(FLOAT.ordinal)
                    writer.writeFloatArray(data)
                }
                is DoubleArray -> {
                    writer.writeLength(data.size)
                    writer.writeByte(DOUBLE.ordinal)
                    writer.writeDoubleArray(data)
                }
                else -> {
                    check(data is Array<*>)
                    writer.writeLength(data.size)

                    val type = writer.getDataTypeClass(data::class.java.componentType)
                    writer.writeByte(type.ordinal)
                    for (entry in data) {
                        type.write(writer, entry)
                    }
                }
            }
        },
    ),
    MIXED_ARRAY(
        {
            val array: Array<Any?> = arrayOfNulls(it.readLength())
            for (i in array.indices) {
                array[i] = it.readMBFEntry() ?: continue
            }
            array
        },
        { writer, data ->
            check(data is Array<*>)
            writer.writeLength(data.size)
            for (entry in data) {
                writer.writeMBF(entry)
            }
        },
    ),

    NORMAL_LIST(
        {
            val length = it.readLength()
            val type = it.readMBFDataType()
            val list = mutableListOf<Any>()

            for (i in 0 until length) {
                list += type.reader(it) ?: continue
            }
            list
        },
        { writer, data ->
            check(data is List<*>)
            writer.writeLength(data.size)
            val type = writer.getDataTypeClass(data.generics[0])
            writer.writeByte(type.ordinal)
            for (entry in data) {
                type.write(writer, entry)
            }
        },
    ),
    MIXED_LIST(
        {
            val list: MutableList<Any> = mutableListOf()
            for (i in 0 until it.readLength()) {
                list += it.readMBFEntry() ?: continue
            }
            list
        },
        { writer, data ->
            check(data is List<*>)
            writer.writeLength(data.size)
            for (entry in data) {
                writer.writeMBF(entry)
            }
        },
    ),

    NORMAL_SET(
        {
            val length = it.readLength()
            val type = it.readMBFDataType()
            val set = mutableSetOf<Any>()

            for (i in 0 until length) {
                set += type.reader(it) ?: continue
            }
            set
        },
        { writer, data ->
            check(data is Set<*>)
            writer.writeLength(data.size)
            val type = writer.getDataTypeClass(data.generics[0])
            writer.writeByte(type.ordinal)

            for (entry in data) {
                type.write(writer, entry)
            }
        }
    ),
    MIXED_SET(
        {
            val set: MutableSet<Any> = mutableSetOf()
            for (i in 0 until it.readLength()) {
                set += it.readMBFEntry() ?: continue
            }
            set
        },
        { writer, data ->
            check(data is Set<*>)
            writer.writeLength(data.size)
            for (entry in data) {
                writer.writeMBF(entry)
            }
        }
    ),

    NORMAL_MAP(
        {
            val length = it.readLength()
            val keyType = it.readMBFDataType()
            val valueType = it.readMBFDataType()
            val map = mutableMapOf<Any, Any>()

            for (i in 0 until length) {
                val key = keyType.reader(it)
                val value = valueType.reader(it)
                key ?: continue
                value ?: continue
                map[key] = value
            }
            map
        },
        { writer, data ->
            check(data is Map<*, *>)
            writer.writeLength(data.size)
            val keyType = writer.getDataTypeClass(data.generics[0])
            val valueType = writer.getDataTypeClass(data.generics[1])
            writer.writeByte(keyType.ordinal)
            writer.writeByte(valueType.ordinal)
            for ((key, value) in data) {
                keyType.write(writer, key)
                valueType.write(writer, value)
            }
        }
    ),
    MIXED_MAP(
        {
            val map: MutableMap<Any, Any> = mutableMapOf()
            for (i in 0 until it.readLength()) {
                val key = it.readMBFEntry()
                val value = it.readMBFEntry()
                key ?: continue
                value ?: continue
                map[key] = value
            }
            map
        },
        { writer, data ->
            check(data is Map<*, *>)
            writer.writeLength(data.size)
            for ((key, value) in data) {
                writer.writeMBF(key)
                writer.writeMBF(value)
            }
        },
    ),

    VAR_LONG(
        { it.readVarLong() },
        { writer, data -> writer.writeVarLong(data as Long) },
    ),

    ;

    companion object {
        val VALUES = values()
        // private val MUTABLE_LIST_ADD_METHOD = MutableList::class.java.getMethod("add", Any::class.java)
    }
}
